# Qualifinds Backend Developer Challenge

A test for a job application at Qualifinds, based in building a REST API that implements CRUD, using Node with Express and Sequelize for PostgreSQL.

Is a system based on Posts with Reviews sections, all of them managed by Users with distinct privileges for each operation.

## Requirements

- Linux based system (Tested in Fedora 33)
- Node.js v14
- NPM v6
- PostgreSQL v13

### Installing

Download the project:

```
git clone git@gitlab.com:rgbeurie/qualifinds-back-end-developer-challenge.git
```

Start your PostgreSQL server. Check with:

```
sudo systemctl status postgresql-13

```
If is inactive, run:

```
sudo systemctl start postgresql-13
```

Locate in the project folder. Set the '.env' file with your systems parameters, and then populate the database with:

```
npm run seed
```

Install the packages dependencies of the project:

```
npm i
```

Now you can start the server with:

```
node server.js
```

or with:

```
npx nodemon
```

### Test

The test uses the `admin`credentials and the example post with the URI `Lorem%20Ipsum` provided by the `seed`. So, if you have run it the `seed` script previously, there is no extra step and you can continue with the test, if is not the case, it is a good chance to do it.

Also, if you have gotten the repository before this update, is necessary to run the package dependencies installation command again:

```
npm i
```

Now, the corresponding command to run the test:

```
npm test
```

You should observe that the code has a total of `13 passed` tests to ensure that everything is right.

### Check

You can use a Postman collection located in a folder with its own name. There is every possible request to make to the server.

Have fun :)

## Authors

Reyes Gaytán Beurie G.

