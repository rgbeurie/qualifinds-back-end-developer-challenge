import { config } from 'dotenv'
import app from './app/index.js'

// Load .env
config()

app.listen(process.env.SERVERPORT, err => {
  if (err) console.error(err)
  else console.log(`server listening on port: ${process.env.SERVERPORT}`)
})
