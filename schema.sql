--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: posts; Type: TABLE; Schema: public; Owner: qualifinds
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    uri character varying(255) NOT NULL,
    content text NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "userId" integer
);


ALTER TABLE public.posts OWNER TO qualifinds;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: qualifinds
--

CREATE SEQUENCE public.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO qualifinds;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qualifinds
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: registers; Type: TABLE; Schema: public; Owner: qualifinds
--

CREATE TABLE public.registers (
    id integer NOT NULL,
    action character varying(6) NOT NULL,
    date timestamp with time zone NOT NULL,
    "userId" integer,
    "postId" integer
);


ALTER TABLE public.registers OWNER TO qualifinds;

--
-- Name: registers_id_seq; Type: SEQUENCE; Schema: public; Owner: qualifinds
--

CREATE SEQUENCE public.registers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registers_id_seq OWNER TO qualifinds;

--
-- Name: registers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qualifinds
--

ALTER SEQUENCE public.registers_id_seq OWNED BY public.registers.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: qualifinds
--

CREATE TABLE public.reviews (
    id integer NOT NULL,
    content text NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "userId" integer,
    "postId" integer
);


ALTER TABLE public.reviews OWNER TO qualifinds;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: qualifinds
--

CREATE SEQUENCE public.reviews_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO qualifinds;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qualifinds
--

ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: qualifinds
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(18) NOT NULL,
    password character varying(60) NOT NULL,
    email character varying(255) NOT NULL,
    admin boolean DEFAULT false,
    make boolean DEFAULT false,
    edit boolean DEFAULT false,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);


ALTER TABLE public.users OWNER TO qualifinds;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: qualifinds
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO qualifinds;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: qualifinds
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: registers id; Type: DEFAULT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.registers ALTER COLUMN id SET DEFAULT nextval('public.registers_id_seq'::regclass);


--
-- Name: reviews id; Type: DEFAULT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: qualifinds
--

COPY public.posts (id, title, uri, content, "createdAt", "updatedAt", "deletedAt", "userId") FROM stdin;
1	Lorem Ipsum	Lorem%20Ipsum	Lorem ipsum dolor sit amet, consectetur adipiscing elit.	2021-03-01 00:00:00-06	2021-03-01 00:00:00-06	\N	1
\.


--
-- Data for Name: registers; Type: TABLE DATA; Schema: public; Owner: qualifinds
--

COPY public.registers (id, action, date, "userId", "postId") FROM stdin;
1	create	2021-03-01 00:00:00-06	1	1
\.


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: qualifinds
--

COPY public.reviews (id, content, "createdAt", "updatedAt", "deletedAt", "userId", "postId") FROM stdin;
1	Praesent varius aliquet lectus sed auctor.	2021-03-01 00:00:00-06	2021-03-01 00:00:00-06	\N	1	1
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: qualifinds
--

COPY public.users (id, name, password, email, admin, make, edit, "createdAt", "updatedAt", "deletedAt") FROM stdin;
1	admin	$2a$10$h8eQd2AE0i3E8AfakcN30ukeWBe7mgqCrCFWFvbtG4WpBEwmxwbu6	admin@email.com	t	f	f	2021-03-01 00:00:00-06	2021-03-01 00:00:00-06	\N
\.


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: qualifinds
--

SELECT pg_catalog.setval('public.posts_id_seq', 1, true);


--
-- Name: registers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: qualifinds
--

SELECT pg_catalog.setval('public.registers_id_seq', 1, true);


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: qualifinds
--

SELECT pg_catalog.setval('public.reviews_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: qualifinds
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: registers registers_pkey; Type: CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.registers
    ADD CONSTRAINT registers_pkey PRIMARY KEY (id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: posts posts_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT "posts_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: registers registers_postId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.registers
    ADD CONSTRAINT "registers_postId_fkey" FOREIGN KEY ("postId") REFERENCES public.posts(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: registers registers_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.registers
    ADD CONSTRAINT "registers_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: reviews reviews_postId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT "reviews_postId_fkey" FOREIGN KEY ("postId") REFERENCES public.posts(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: reviews reviews_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: qualifinds
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT "reviews_userId_fkey" FOREIGN KEY ("userId") REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

