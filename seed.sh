source ./.env

# create database
psql postgres://$PGUSER:$PGPASSWORD@$PGHOST:$PGPORT/postgres -c "CREATE DATABASE $PGDATABASE;"

# create schema and data
psql postgres://$PGUSER:$PGPASSWORD@$PGHOST:$PGPORT/$PGDATABASE -f ./schema.sql
