import express from 'express'
import auth from './routes/auth.js'
import user from './routes/user.js'
import post from './routes/post.js'
import review from './routes/review.js'
import register from './routes/register.js'

const app = express()

// Parse incoming request to JSON
app.use(express.json())

// Define routes
app.use('/auth', auth)
app.use('/user', user)
app.use('/post', post)
app.use('/review', review)
app.use('/register', register)

// Fallbacks for routes not found and catched errors
app.use((req, res) => res.sendStatus(404))
app.use((err, req, res, next) => {
  console.error(err)
  res.status(500)
  return res.send(err.message)
})

export default app
