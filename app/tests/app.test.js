import supertest from 'supertest'
import app from '../index.js'

describe('app', () => {
  const username = `admin`
  const password = `admin`
  const postUri = `Lorem%20Ipsum`
  const timestamp = (new Date()).toISOString().replace(/[-:.TZ]/g,'')
  let token

  test(`Login should respond with a token.`, async(done) => {
    const response = await supertest(app)
      .post('/auth/login/')
      .send({ user: username, password: password })

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('token')

    token = response?.body?.token

    done()
  })
  
  test(`User creation should respond with the user given without password.`, async(done) => {
    const user = {
      name: timestamp,
      password: `Abcd-1234`,
      email: `${timestamp}@mail.com`,
      admin: false,
      make: false,
      edit: false,
    }

    const response = await supertest(app).post(`/user/`)
      .set('Authorization', `Bearer ${token}`)
      .send(user)

    delete user.password

    expect(response.statusCode).toBe(200)
    expect(response.body).toMatchObject(user)
    expect(response.body).toHaveProperty('createdAt')

    done()
  })
  
  test(`User read all should respond with valid users.`, async(done) => {
    const response = await supertest(app).get(`/user/`)
      .set('Authorization', `Bearer ${token}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).not.toHaveLength(0)
    expect(response.body[0]).toHaveProperty('name')
    expect(response.body[0]).toHaveProperty('email')
    expect(response.body[0]).toHaveProperty('admin')
    expect(response.body[0]).toHaveProperty('make')
    expect(response.body[0]).toHaveProperty('edit')
    
    done()
  })
  
  test(`User read should respond with the user requested.`, async(done) => {
    const response = await supertest(app).get(`/user/${username}`)
      .set('Authorization', `Bearer ${token}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('name', `admin`)
    expect(response.body).toHaveProperty('admin', true)
    expect(response.body).toHaveProperty('email')
    expect(response.body).toHaveProperty('make')
    expect(response.body).toHaveProperty('edit')
    
    done()
  })
  
  test(`User update should respond with the user updated.`, async(done) => {
    let userDataToUpdate = {
      admin: true,
      make: false,
      edit: false,
    }
    
    const response = await supertest(app).put(`/user/${username}`)
      .set('Authorization', `Bearer ${token}`)
      .send(userDataToUpdate)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('name', `admin`)
    expect(response.body).toHaveProperty('admin', true)
    expect(response.body).toHaveProperty('make', false)
    expect(response.body).toHaveProperty('edit', false)
    expect(response.body).toHaveProperty('updatedAt')
    
    done()
  })

  test(`User delete should respond indicating that the user has been deleted.`, async(done) => {
    const response = await supertest(app).delete(`/user/${timestamp}`)
      .set('Authorization', `Bearer ${token}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('name', `${timestamp}`)
    expect(response.body).toHaveProperty('deletedAt')
    
    done()
  })

  test(`Post creation should respond with the post given.`, async(done) => {
    const post = {
      title: timestamp,
      content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.`,
    }

    const response = await supertest(app).post(`/post/`)
      .set('Authorization', `Bearer ${token}`)
      .send(post)

    expect(response.statusCode).toBe(200)
    expect(response.body).toMatchObject(post)
    expect(response.body).toHaveProperty('createdAt')

    done()
  })
  
  test(`Post read all should respond with valid posts.`, async(done) => {
    const response = await supertest(app).get(`/post/`)

    expect(response.statusCode).toBe(200)
    expect(response.body).not.toHaveLength(0)
    expect(response.body[0]).toHaveProperty('title')
    expect(response.body[0]).toHaveProperty('uri')
    expect(response.body[0]).toHaveProperty('content')
    expect(response.body[0]).toHaveProperty('user')
    expect(response.body[0]).toHaveProperty('reviews')
    
    done()
  })

  test(`Post read should respond with the post requested.`, async(done) => {
    const response = await supertest(app).get(`/post/${postUri}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('uri', `${postUri}`)
    expect(response.body).toHaveProperty('title')
    expect(response.body).toHaveProperty('content')
    expect(response.body).toHaveProperty('user')
    expect(response.body).toHaveProperty('reviews')
    
    done()
  })

  test(`Post update should respond with the post updated.`, async(done) => {
    let postDataToUpdate = {
      content: `${timestamp}`,
    }
    
    const response = await supertest(app).put(`/post/${postUri}`)
      .set('Authorization', `Bearer ${token}`)
      .send(postDataToUpdate)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('uri', `${postUri}`)
    expect(response.body).toHaveProperty('content', `${timestamp}`)
    expect(response.body).toHaveProperty('title')
    expect(response.body).toHaveProperty('user')
    expect(response.body).toHaveProperty('reviews')
    expect(response.body).toHaveProperty('updatedAt')
    
    done()
  })

  test(`Post delete should respond indicating that the post has been deleted.`, async(done) => {
    const response = await supertest(app).delete(`/post/${timestamp}`)
      .set('Authorization', `Bearer ${token}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).toHaveProperty('title', `${timestamp}`)
    expect(response.body).toHaveProperty('deletedAt')
    
    done()
  })

  test(`Review creation should respond with the review given.`, async(done) => {
    const review = {
      content: `${timestamp}`,
    }

    const response = await supertest(app).post(`/review/${postUri}`)
      .send(review)

    expect(response.statusCode).toBe(200)
    expect(response.body).toMatchObject(review)
    expect(response.body).toHaveProperty('createdAt')

    done()
  })

  test(`Register read all should respond with valid registers.`, async(done) => {
    const response = await supertest(app).get(`/register/`)
      .set('Authorization', `Bearer ${token}`)

    expect(response.statusCode).toBe(200)
    expect(response.body).not.toHaveLength(0)
    expect(response.body[0]).toHaveProperty('action')
    expect(response.body[0]).toHaveProperty('date')
    expect(response.body[0]).toHaveProperty('user')
    expect(response.body[0]).toHaveProperty('post')
    
    done()
  })
})
