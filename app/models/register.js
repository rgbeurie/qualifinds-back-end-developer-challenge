import Sequelize from 'sequelize'
const { Model, STRING, DATE, NOW } = Sequelize

class Register extends Model {}

/**
 * Model definitions for Registers Table
 * @param {Object} database Sequelize Instance initiated
 * @returns {Object} Register Class ready to use for Database operations
 */
function init(database) {
  Register.init({
    action: {
      type: STRING(6),
      allowNull: false,
    },
    date: {
      type: DATE,
      allowNull: false,
      defaultValue: NOW,
    },
  }, { sequelize: database, modelName: 'registers', timestamps: false }) // Timestamps disabled

  return Register
}

export default init
