import Sequelize from 'sequelize'
const { Model, STRING, BOOLEAN } = Sequelize

class User extends Model {}

/**
 * Model definitions for Users Table
 * @param {Object} database Sequelize Instance initiated
 * @returns {Object} User Class ready to use for Database operations
 */
function init(database) {
  User.init({
    name: {
      type: STRING(18),
      allowNull: false,
    },
    password: {
      type: STRING(60),
      allowNull: false,
    },
    email: {
      type: STRING,
      allowNull: false,
    },
    admin: {
      type: BOOLEAN,
      defaultValue: false,
    },
    make: {
      type: BOOLEAN,
      defaultValue: false,
    },
    edit: {
      type: BOOLEAN,
      defaultValue: false,
    },
  }, { sequelize: database, modelName: 'users', paranoid: true }) // Soft deletion enabled

  return User
}

export default init
