import Sequelize from 'sequelize'
const { Model, TEXT } = Sequelize

class Review extends Model {}

/**
 * Model definitions for Reviews Table
 * @param {Object} database Sequelize Instance initiated
 * @returns {Object} Review Class ready to use for Database operations
 */
function init(database) {
  Review.init({
    content: {
      type: TEXT,
      allowNull: false,
    },
  }, { sequelize: database, modelName: 'reviews', paranoid: true }) // Soft deletion enabled

  return Review
}

export default init
