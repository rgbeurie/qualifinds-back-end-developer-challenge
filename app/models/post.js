import Sequelize from 'sequelize'
const { Model, STRING, TEXT } = Sequelize

class Post extends Model {}

/**
 * Model definitions for Posts Table
 * @param {Object} database Sequelize Instance initiated
 * @returns {Object} Post Class ready to use for Database operations
 */
function init(database) {
  Post.init({
    title: {
      type: STRING(100),
      allowNull: false,
    },
    uri: {
      type: STRING,
      allowNull: false,
    },
    content: {
      type: TEXT,
      allowNull: false,
    },
  }, { sequelize: database, modelName: 'posts', paranoid: true }) // Soft deletion enabled

  return Post
}

export default init
