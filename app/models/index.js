import { config } from 'dotenv'
import Sequelize from 'sequelize'
import UserModel from '../models/user.js'
import PostModel from '../models/post.js'
import ReviewModel from '../models/review.js'
import RegisterModel from '../models/register.js'

// Load .env for Database credentials
config()

// Set PostgreSQL as a Database language and disable Database logs
const database = new Sequelize({ dialect: 'postgres', logging: false })

// Attempt connection to Database
database.authenticate()
.then(() => console.log('database connection successful'))
.catch((err) => console.error('database connection unsuccessful:', err))

// Load Models for operations to the Database
const User = UserModel(database)
const Post = PostModel(database)
const Review = ReviewModel(database)
const Register = RegisterModel(database)

// Define Relations between Models
User.hasMany(Post)
Post.belongsTo(User)

User.hasMany(Review)
Review.belongsTo(User)

User.hasMany(Register)
Register.belongsTo(User)

Post.hasMany(Review)
Review.belongsTo(Post)

Post.hasMany(Register)
Register.belongsTo(Post)

// Synchronize definitions between Models and Database
database.sync()

export { User, Post, Review, Register }
