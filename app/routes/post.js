import { Router } from 'express'
import { identify, forbid } from '../middlewares/auth.js'
import { create, findAll, findOne, save, destroy } from '../controllers/post.js'

const router = Router()

// JWT identification, not mandatory until "forbid" define control access
router.use(identify)

// Route definition for CRUD to Posts
router.post('/', forbid('make'), create)
router.get('/', findAll)
router.get('/:uri', findOne)
router.put('/:uri', forbid('edit'), save)
router.delete('/:uri', forbid('make'), destroy)

export default router
