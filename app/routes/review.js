import { Router } from 'express'
import { identify } from '../middlewares/auth.js'
import { create } from '../controllers/review.js'

const router = Router()

// JWT identification, not mandatory until "forbid" define control access
router.use(identify)

// Route definition for Create Reviews
router.post('/:uri', create)

export default router
