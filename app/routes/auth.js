import { Router } from 'express'
import { login } from '../controllers/user.js'

const router = Router()

// Route definition for login
router.post('/login', login)

export default router
