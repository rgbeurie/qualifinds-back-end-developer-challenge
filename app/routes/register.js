import { Router } from 'express'
import { identify, forbid } from '../middlewares/auth.js'
import { findAll } from '../controllers/register.js'

const router = Router()

// JWT identification, not mandatory until "forbid" define control access
router.use(identify)
router.use(forbid())

// Route definition for Read all Registers
router.get('/', findAll)

export default router
