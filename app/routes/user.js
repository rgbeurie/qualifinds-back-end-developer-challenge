import { Router } from 'express'
import { identify, forbid } from '../middlewares/auth.js'
import { create, findAll, findOne, save, destroy } from '../controllers/user.js'

const router = Router()

// JWT identification, not mandatory until "forbid" define control access
router.use(identify)
router.use(forbid())

// Route definition for CRUD to Users
router.post('/', create)
router.get('/', findAll)
router.get('/:name', findOne)
router.put('/:name', save)
router.delete('/:name', destroy)

export default router
