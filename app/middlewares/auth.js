import jwt from 'jsonwebtoken'
import validator from 'validator'
import { promisify } from 'util'
import { User } from '../models/index.js'

/**
 * Identification for JWT. If success it will set the identification to the Response Locals
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function identify(req, res, next) {
  try {
    if(!req.headers.authorization || !validator.isJWT(req.headers.authorization.replace("Bearer ", ""))) return next()

    let payload

    try {
      payload = await promisify(jwt.verify)(req.headers.authorization.replace("Bearer ", ""), process.env.JWTKEY)
    } catch(err) { return next() }

    const user = await User.findByPk(payload.id, { attributes: { exclude: ['password'] } })

    if(!user) return next()

    res.locals.user = user

    return next()
  } catch(err) { return next(err) }
}

/**
 * Establish privileges restrictions. If not provided only allow 'admin' privileges
 * @param {String} privileges Definition for privileges restrictions. Possible options 'make' or 'edit'
 * @returns {Function} Will return a middleware function
 */
function forbid(privileges) {
  return (req, res, next) => {
    try {
      if(res.locals?.user?.admin) return next()
      if(privileges === 'make' && res.locals?.user?.make) return next()
      if(privileges === 'edit' && res.locals?.user?.edit) return next()

      return res.sendStatus(401)
    } catch(err) { return next(err) }
  }
}

export { identify, forbid }
