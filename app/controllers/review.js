import { Review, Post } from '../models/index.js'

/**
 * Create new Instance for Reviews
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function create(req, res, next) {
  try {
    const post = await Post.findOne({ where: { title: req.params.uri } })

    if(!post) return res.sendStatus(404)

    let validity = ``

    // Body Request validations
    if(!req.body.content) validity += `content should be provided. `
    else if(typeof req.body.content !== 'string') validity += `content should be string. `

    if(validity) {
      res.status(400)
      return res.send(validity)
    }

    // Parse data for presentation
    let review = await Review.create({ content: req.body.content, postId: post.id, userId: res.locals?.user?.id || null })

    review = review.toJSON()

    delete review.id
    delete review.postId
    delete review.userId

    return res.send(review)
  } catch(err) { return next(err) }
}

export { create }
