import { Register, User, Post } from '../models/index.js'

/**
 * Find all Instances of Registers
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function findAll(req, res, next) {
  try {
    let registers = await Register.findAll({
      order: [['date', 'DESC']],
      attributes: { exclude: ['id', 'userId', 'postId'] },
      include: [
        {
          model: User,
          paranoid: false,
        },
        {
          model: Post,
          paranoid: false, // Get even soft deleted data
        }
      ],
    })

    // Parse data for presentation
    registers = registers.map(register => {
      register = register.toJSON()
      register.user = register.user.name
      register.post = register.post?.title

      return register
    })

    return res.send(registers)
  } catch(err) { return next(err) }
}
export { findAll }
