import jwt from 'jsonwebtoken'
import bcrypt from 'bcryptjs'
import validator from 'validator'
import Sequelize from 'sequelize'
import { promisify } from 'util'
import { User } from '../models/index.js'
const { Op } = Sequelize

/**
 * Login for Users
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function login(req, res, next) {
  try {
    if(!req.body.user || !req.body.password) return res.sendStatus(400)

    const user = await User.findOne({ where: { name: req.body.user } })

    if(!user) return res.sendStatus(401)

    const valid = await bcrypt.compare(req.body.password, user.password)

    if(!valid) return res.sendStatus(401)

    const payload = { id: user.id }

    const token = await promisify(jwt.sign)(payload, process.env.JWTKEY, { noTimestamp: true, expiresIn: '1h' })

    return res.send({token})
  } catch(err) { return next(err) }
}

/**
 * Create new Instance for Users
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function create(req, res, next) {
  try {
    let validity = ``

    // Body Request validations
    if(!req.body.name) validity += `name should be provided. `
    else if(typeof req.body.name !== 'string') validity += `name should be string. `
    else if(!validator.isLength(req.body.name, { min: 1, max: 18 })) validity += `name should be between 1 and 18 of length. `
    else if(!validator.isAlphanumeric(req.body.name)) validity += `name should be alphanumeric. `
    if(!req.body.password) validity += `password should be provided. `
    if(typeof req.body.password !== 'string') validity += `password should be string. `
    else if(!validator.isStrongPassword(req.body.password)) validity += `password should be a minimum 8 characters with at least 1 lower case, 1 upper case, 1 number, and 1 symbol. `
    if(!req.body.email) validity += `email should be provided. `
    else if(typeof req.body.email !== 'string') validity += `email should be string. `
    else if(!validator.isEmail(req.body.email)) validity += `email should be valid. `
    if(typeof req.body?.admin !== 'boolean') validity += `admin privileges should be boolean. `
    if(typeof req.body?.make !== 'boolean') validity += `make posts privileges should be boolean. `
    if(typeof req.body?.edit !== 'boolean') validity += `edit posts privileges should be boolean. `

    if(validity) {
      res.status(400)
      return res.send(validity)
    }

    const userToCreate = {}
    
    // Data preparation for the new Instance
    userToCreate.name = req.body.name.toLowerCase()
    userToCreate.password = await bcrypt.hash(req.body.password.trim(), 10)
    userToCreate.email = req.body.email.toLowerCase()

    if(req.body.admin) userToCreate.admin = req.body.admin
    if(req.body.make) userToCreate.make = req.body.make
    if(req.body.edit) userToCreate.edit = req.body.edit

    // Find for duplicates, if not found the Instance will be created
    let [user, created] = await User.findOrCreate({
      where: { [Op.or]: [{ name: userToCreate.name }, { email: userToCreate.email }] },
      defaults: userToCreate,
    })

    if(!created) {
      validity = ``

      if(user.name === userToCreate.name) validity += `cannot use that user name. `
      if(user.email === userToCreate.email) validity += `cannot use that email. `
      
      res.status(409)
      return res.send(validity)
    }

    user = user.toJSON()

    delete user.id
    delete user.password

    return res.send(user)
  } catch(err) { return next(err) }
}

/**
 * Find all Instances of Users
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function findAll(req, res, next) {
  try {
    const users = await User.findAll({ attributes: { exclude: ['id', 'password'] } })

    return res.send(users)
  } catch(err) { return next(err) }
}

/**
 * Find an Instance of User
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function findOne(req, res, next) {
  try {
    if(!validator.isAlphanumeric(req.params.name)) return res.sendStatus(400)

    const user = await User.findOne({ where: { name: req.params.name }, attributes: { exclude: ['id', 'password'] } })

    if(!user) return res.sendStatus(404)

    return res.send(user)
  } catch(err) { return next(err) }
}

/**
 * Save changes to an existing Instance of Users
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function save(req, res, next) {
  try {
    if(!validator.isAlphanumeric(req.params.name)) return res.sendStatus(400)

    let user = await User.findOne({ where: { name: req.params.name }, attributes: { exclude: ['password'] } })

    if(!user) return res.sendStatus(404)

    let validity = ``

    // Body Request validations
    if(req.body.name !== undefined) validity += `name cannot be modified. `
    if(req.body.email !== undefined) validity += `email cannot be modified. `

    if(req.body.password !== undefined) {
      if(typeof req.body.password !== 'string') validity += `password should be string. `
      else if(!validator.isStrongPassword(req.body.password)) validity += `password should be a minimum 8 characters with at least 1 lower case, 1 upper case, 1 number, and 1 symbol. `
      else user.password = await bcrypt.hash(req.body.password, 10)
    }

    if(req.body.admin !== undefined) {
      if(typeof req.body.admin !== 'boolean') validity += `admin privileges should be boolean. `
      else user.admin = req.body.admin
    }

    if(req.body.make !== undefined) {
      if(typeof req.body.make !== 'boolean') validity += `make posts privileges should be boolean. `
      else user.make = req.body.make
    }

    if(req.body.edit !== undefined) {
      if(typeof req.body.edit !== 'boolean') validity += `edit posts privileges should be boolean. `
      else user.edit = req.body.edit
    }

    if(validity) {
      res.status(400)
      return res.send(validity)
    }

    await user.save()

    // Parse data for presentation
    user = user.toJSON()

    delete user.id
    delete user.password

    return res.send(user)
  } catch(err) { return next(err) }
}

/**
 * Destroy an Instance of Users
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function destroy(req, res, next) {
  try {
    if(!validator.isAlphanumeric(req.params.name)) return res.sendStatus(400)

    let user = await User.findOne({ where: { name: req.params.name }, attributes: { exclude: ['password'] } })

    if(!user) return res.sendStatus(404)

    if(user.id === res.locals.user.id) {
      res.status(405)
      return res.send(`cannot autodestroy`)
    }

    await user.destroy()

    user = user.toJSON()

    delete user.id

    return res.send(user)
  } catch(err) { return next(err) }
}

export { login, create, findAll, findOne, save, destroy }
