import validator from 'validator'
import Sequelize from 'sequelize'
import { Post, Review, Register } from '../models/index.js'
const { Op } = Sequelize

/**
 * Create new Instance for Posts
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function create(req, res, next) {
  try {
    let validity = ``

    // Body Request validations
    if(!req.body.title) validity += `title should be provided. `
    else if(typeof req.body.title !== 'string') validity += `title should be string. `
    else if(!validator.isLength(req.body.title, { min: 1, max: 100 })) validity += `title should be between 1 and 100 of length. `
    if(!req.body.content) validity += `content should be provided. `
    else if(typeof req.body.content !== 'string') validity += `content should be string. `

    if(validity) {
      res.status(400)
      return res.send(validity)
    }

    const postToCreate = {}
    
    // Data preparation for the new Instance
    postToCreate.title = req.body.title
    postToCreate.uri = encodeURIComponent(req.body.title)
    postToCreate.content = req.body.content
    postToCreate.userId = res.locals.user.id

    // Find for duplicates, if not found the Instance will be created
    let [post, created] = await Post.findOrCreate({
      where: { title: postToCreate.title },
      defaults: postToCreate,
    })

    if(!created) {
      validity = ``

      if(post.title === postToCreate.title) validity += `cannot use that title. `
      
      res.status(409)
      return res.send(validity)
    }

    // Register the action in Registers
    await Register.create({ action: 'create', userId: res.locals.user.id, postId: post.id })

    post = post.toJSON()

    delete post.id
    delete post.userId

    return res.send(post)
  } catch(err) { return next(err) }
}

/**
 * Find all Instances of Posts
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function findAll(req, res, next) {
  try {
    let validity = ``
    let query = {}

    // Query Request validations
    if(req.query.from && req.query.to) {
      if(!validator.isDate(req.query.from)) validity += `"from" should be a date in a format YYYY-MM-DD. `
      if(!validator.isDate(req.query.to)) validity += `"to" should be a date in a format YYYY-MM-DD. `
      if(new Date(req.query.from) > new Date(req.query.to)) validity += `"from" date should be before "to" date. `

      if(validity) {
        res.status(400)
        return res.send(validity)
      }

      query = { 'createdAt': { [Op.gte]: new Date(req.query.from) } }
    }

    // Find Instances and use query if is filled
    let posts = await Post.findAll({
      where: query,
      order: [['createdAt', 'DESC'], [Review, 'createdAt', 'desc']],
      attributes: { exclude: ['id', 'userId'] },
      include: [
        'user',
        {
          model: Review,
          include: ['user'],
        }
      ],
    })
    
    // Parse data for presentation
    posts = posts.map(post => {
      post = post.toJSON()
      post.user = post.user.name

      post.reviews = post.reviews.map(review => {        
        review.user = review?.user?.name || 'anonymous'

        delete review.id
        delete review.updatedAt
        delete review.deletedAt
        delete review.userId
        delete review.postId

        return review
      })

      if(Object.keys(query).length && post.createdAt > new Date(req.query.to)) post.newer = true

      return post
    })

    return res.send(posts)
  } catch(err) { return next(err) }
}

/**
 * Find an Instance of Posts
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function findOne(req, res, next) {
  try {
    let post = await Post.findOne({
      where: { title: req.params.uri },
      order: [[Review, 'createdAt', 'desc']],
      attributes: { exclude: ['id', 'userId'] },
      include: [
        'user',
        {
          model: Review,
          include: ['user'],
        }
      ],
    })

    if(!post) return res.sendStatus(404)

    // Parse data for presentation
    post = post.toJSON()
    post.user = post.user.name

    post.reviews = post.reviews.map(review => {     
      review.user = review?.user?.name || 'anonymous'

      delete review.id
      delete review.updatedAt
      delete review.deletedAt
      delete review.userId
      delete review.postId

      return review
    })

    return res.send(post)
  } catch(err) { return next(err) }
}

/**
 * Save changes to an existing Instance of Posts
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function save(req, res, next) {
  try {
    let post = await Post.findOne({
      where: { title: req.params.uri },
      order: [[Review, 'createdAt', 'desc']],
      attributes: { exclude: ['userId'] },
      include: [
        'user',
        {
          model: Review,
          include: ['user'],
        }
      ],
    })

    if(!post) return res.sendStatus(404)

    let validity = ``

    // Body Request validations
    if(req.body.title !== undefined) validity += `title cannot be modified. `
    if(req.body.uri !== undefined) validity += `uri cannot be modified. `

    if(!req.body.content) validity += `content should be provided. `
    else if(typeof req.body.content !== 'string') validity += `content should be string. `

    if(validity) {
      res.status(400)
      return res.send(validity)
    }

    post.content = req.body.content

    // Save the Instance and register the action in Registers
    await post.save()
    await Register.create({ action: 'update', userId: res.locals.user.id, postId: post.id })

    // Parse data for presentation
    post = post.toJSON()
    post.user = post.user.name

    post.reviews = post.reviews.map(review => {        
      review.user = review?.user?.name || 'anonymous'

      delete review.id
      delete review.updatedAt
      delete review.deletedAt
      delete review.userId
      delete review.postId

      return review
    })

    delete post.id
    delete post.userId

    return res.send(post)
  } catch(err) { return next(err) }
}

/**
 * Destroy an Instance of Posts
 * @param {Object} req Request Express Object where the Request is coming
 * @param {Object} res Response Express Object where the Response will go
 * @param {Object} next Next Express Object to pass to the next function of the flow
 * @returns {Object} Express Response to the Client. If fails the flow continues to the fallback for Catched Errors
 */
async function destroy(req, res, next) {
  try {
    let post = await Post.findOne({ where: { title: req.params.uri } })

    if(!post) return res.sendStatus(404)

    // Destroy the Instance and register the action in Registers
    await post.destroy()
    await Register.create({ action: 'delete', userId: res.locals.user.id, postId: post.id })

    post = post.toJSON()

    delete post.id

    return res.send(post)
  } catch(err) { return next(err) }
}

export { create, findAll, findOne, save, destroy }
